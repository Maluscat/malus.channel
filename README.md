# Malus.zone

My personal website, a portfolio and project hub.

It's a simple, conventionally built site without any fancy frameworks,
running on Nodejs and using [Express](https://expressjs.com/) for routing
and [Nunjucks](https://mozilla.github.io/nunjucks/) for templating.

This website has been my first dabble in web development and acted as a monorepo.
Now, after a lot of makeovers, it mostly hosts projects that have their own repo.
Here's a (probably outdated) list:
- [Scratchet](https://gitlab.com/Maluscat/scratchet) (TBA)
- [Slider89](https://github.com/Maluscat/Slider89) (TBA)
- [Win95-Web](https://github.com/Maluscat/win95-web)
- [Snake](https://github.com/Maluscat/snake)
- [Graphics Experiments](https://gitlab.com/Maluscat/graphics-experiments)


## Setup
If you ever want to set this project up locally (why would you), you'll have to:
1. Do a [Yarn](https://yarnpkg.com/) install.
2. Install all Git submodules
3. Symlink or clone Snake and Graphics-Experiments (see above) into the project root

After that, you can start up the website using the Yarn `start` script,
or the `dev` script for a watch mode.

```sh
yarn install
git submodule update --init --recursive
git clone https://github.com/Maluscat/snake
git clone https://gitlab.com/Maluscat/graphics-tools
yarn run start (or `yarn run dev`)
```


## Previous websites
The currently still running [hallo89.net](https://hallo89.net) has been superseded
by malus.zone and frozen in place at commit `0d37ede8`.
