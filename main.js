import fs from 'node:fs/promises';
import path from 'node:path';
import Polka from 'polka';
import Sirv from 'sirv';
import Yaml from 'js-yaml';
import { imageDimensionsFromData } from 'image-dimensions';

import { setupNunjucks, getTemplate } from './nunjucks.js';

// ---- Constants ----
const DEV_MODE = process.env.NODE_ENV === 'development';
const STATIC_DIR = './static';
const VIEW_DIR = './view';
const pageData = Yaml.load(await fs.readFile('./page-data.yml', { encoding: 'utf8' }));


// ---- Web server & Nunjucks setup ----
const app = Polka();
await preprocessPageData(pageData);
setupNunjucks(pageData);

// -- Static resources --
app.use(Sirv('./lib/website-common/static', { dev: DEV_MODE }));
app.use(Sirv('./static', { dev: DEV_MODE }));

// Modules
app.use('/snake', Sirv('./snake/static', { dev: DEV_MODE }));
app.use('/gameoflife', Sirv('./game-of-life/static', { dev: DEV_MODE }));
app.use('/graphics', Sirv('./graphics-experiments/static', { dev: DEV_MODE }));


// ---- Routes ----
getNJK('', './view/index');

getNJK('tools');
getNJK('tools/3DMagic');
getNJK('tools/RFG');
getNJK('tools/mocking');
getNJK('tools/spacing');

getNJK('graphics');
getNJK('graphics/triangles', './graphics-experiments/view/triangles');
getNJK('graphics/matrices3d', './graphics-experiments/view/matrices3d');

getNJK('games');
getNJK('games/snake3D', './snake/snake3D');
getNJK('games/snake2D', './snake/snake2D');

getNJK('games/gameoflife', './game-of-life/view/index');


app.listen(8001, () => {
  console.log("Listening on port ༼ง ◉_◉༽ง 8001!");
});


// ---- Route functions ----
function getNJK(viewPath, filePath = path.join(VIEW_DIR, viewPath), customData) {
  filePath = path.resolve(filePath + '.njk');
  const renderData = {
    pagePath: viewPath,
    pageName: viewPath.includes('/') ? viewPath.slice(viewPath.indexOf('/') + 1) : viewPath,
    pageData: getPageDataFromPath(viewPath),
  };
  if (customData) {
    Object.assign(renderData, customData);
  }
  getTemplate(filePath); // Precompile

  app.get('/' + viewPath, (req, res) => {
    const template = getTemplate(filePath);
    if (template) {
      res.writeHead(200, {
        'Content-Type': 'text/html; charset=utf-8'
      });
      res.end(template.render(renderData));
    } else {
      res.end(`404 not found: /${viewPath}`);
    }
  });
}

function getPageDataFromPath(viewPath) {
  let data = pageData;
  // Special check for index
  if (viewPath !== '') {
    for (const view of viewPath.split('/')) {
      if (data.children) data = data.children;
      data = data[view];
      if (!data) return;
    }
  }
  return data;
}

async function preprocessPageData(data) {
  if (data.image && /[\w-]+\.\w+$/.test(data.image)) {
    const imageBuffer = await fs.readFile(path.join(STATIC_DIR, data.image));
    data.imageSize = imageDimensionsFromData(imageBuffer);
  }
  if (data.children) {
    for (const childData of Object.values(data.children)) {
      preprocessPageData(childData);
    }
  }
}
