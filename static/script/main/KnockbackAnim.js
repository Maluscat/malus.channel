import { AnimationRunner } from './AnimationRunner.js';

/** @typedef { [ number, number ] } Pos */

export class KnockbackAnim extends AnimationRunner {
  // NOTE Sync this with the `gbl-box` timings in `main.less` when changign
  static BASE_DELAY = 665;
  static DURATION = 685;

  #popElements;

  constructor(popElements) {
    super();
    this.#popElements = popElements;
  }

  run(anchorElem, relevantElems) {
    const anchorPos = KnockbackAnim.getCenter(anchorElem);
    return this.runAnchor(anchorPos, relevantElems, anchorElem);
  }

  runAnchor(anchorPos, relevantElems, anchorElem) {
    const animations = [];
    for (const item of relevantElems) {
      if (item === anchorElem) continue;

      const pos = KnockbackAnim.getCenter(item);
      const distance = KnockbackAnim.getDistance(pos, anchorPos);
      const delta = KnockbackAnim.getDelta(pos, anchorPos);

      const keyframe = this.#getKeyframe(item, delta, distance);
      const animation = new Animation(keyframe);
      animations.push(animation);
    }

    return super.run(animations);
  }

  async cleanup(animation) {
    await KnockbackAnim.setElementOpacityAfterAnim(animation, this.#popElements);
  }


  // ---- Static keyframe getter ----
  #getKeyframe(element, delta, distance) {
    distance = Math.pow(distance * 3, .865);

    delta = delta.map(x => {
      const coefficient = .702;
      x *= 0.32;
      return x < 0 ? -Math.pow(x * -1, coefficient) : Math.pow(x, coefficient);
    });

    const delay = distance * .227 + .023;

    const amountStep1 = delta.map(val => val * .775);
    const amountStep2 = delta.map(val => val * -.113);

    const keyframes = [
      {
        translate: 'none',
        easing: 'cubic-bezier(0,0,.35,1)',
      }, {
        translate: `${amountStep1[0]}px ${amountStep1[1]}px`,
        offset: .41,
        easing: 'ease-in-out',
      }, {
        translate: `${amountStep2[0]}px ${amountStep2[1]}px`,
        offset: .83,
        easing: 'ease-in-out',
      },
    ];
    if (this.#popElements.includes(element)) {
      keyframes.splice(1, 0, {
        opacity: 1,
        offset: .12
      });
      keyframes.push({
        opacity: 1,
      });
    }

    const options = {
      delay: delay,
      duration: KnockbackAnim.DURATION,
      easing: 'cubic-bezier(0,0,.55,1)'
    };

    return new KeyframeEffect(element, keyframes, options);
  }

  // --- Misc utility ----
  static async setElementOpacityAfterAnim(animation, elements) {
    if (elements.includes(animation.effect.target)) {
      await animation.finished;
      animation.effect.target.style.opacity = 1;
    }
  }

  // ---- Math/vector utility ----
  /**
   * @param { HTMLElement } item
   * @return { Pos }
   */
  static getCenter(item) {
    item.style.transform = 'none';
    const pos = [
      item.offsetLeft + item.clientWidth / 2,
      item.offsetTop + item.clientHeight / 2,
    ];
    item.style.transform = null;
    return pos;
  }

  /**
   * @param { Pos } pos0
   * @param { Pos } pos1
   * @return { Pos }
   */
  static getDelta(pos0, pos1) {
    return [
      pos0[0] - pos1[0],
      pos0[1] - pos1[1]
    ];
  }

  /**
   * @param { Pos } pos0
   * @param { Pos } pos1
   * @return { number }
   */
  static getDistance(pos0, pos1) {
    return Math.hypot(pos1[0] - pos0[0], pos1[1] - pos0[1]);
  }
}
