import { AnimationRunner } from './AnimationRunner.js';
import { FadeInNarrowAnim } from './FadeInNarrowAnim.js';
import { KnockbackAnim } from './KnockbackAnim.js';
import { BounceAnim } from './BounceAnim.js';

const LAYOUT_WIDTH_STEP_VERTICAL = 570;

const main = document.querySelector('main');
const animatedItems = /** @type { HTMLElement[] } */
  (Array.from(main.querySelectorAll('.main-text > *, .gbl-box, .element')));
const clickableItems = main.querySelectorAll('.main-text > *, .element');
const boxElements = Array.from(main.querySelectorAll('.element'));
const boxes = main.querySelectorAll('.gbl-box');

const mainStyle = getComputedStyle(main);
const gridDims = new Map();

const knockbackAnim = new KnockbackAnim(boxElements);
const fadeInNarrowAnim = new FadeInNarrowAnim(animatedItems);

window.addEventListener('keydown', handleKeyDown);


for (const element of clickableItems) {
  const animation = new BounceAnim(element);
  element.addEventListener('click', itemClick.bind(this, element, animation));
}


init();


function init() {
  if (document.body.clientWidth > LAYOUT_WIDTH_STEP_VERTICAL) {
    initWide();
  } else {
    initNarrow();
  }
}

async function initWide() {
  document.body.classList.add('init-animation');
  // Trigger reflow to ensure that the class has been added
  void document.body.clientHeight;
  document.body.classList.remove('init');
  await AnimationRunner.sleep(KnockbackAnim.BASE_DELAY);
  await knockbackAnim.run(boxes[0], animatedItems);
  document.body.classList.remove('init-animation');
}
async function initNarrow() {
  document.body.classList.remove('init');
  await fadeInNarrowAnim.run();
}


function handleKeyDown(e) {
  if (e.key === 'ScrollLock') {
    document.body.classList.toggle('rainbow-mode');
    if (e.ctrlKey) {
      document.body.classList.toggle('cell-mode');
    }
  }
}

async function itemClick(element, animation, e) {
  animation.run();
  knockbackAnim.run(element, animatedItems);
}
