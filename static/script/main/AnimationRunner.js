export class AnimationRunner {
  #animationList = [];

  run(animations = this.animations) {
    if (this.#animationList.length > 0) {
      this.#animationList.forEach(anim => anim.finish());
      this.#animationList = [];
    }
    for (const animation of animations) {
      this.#animationList.push(animation);
      this.cleanup(animation);
      animation.play();
    }

    return Promise.all(this.#animationList.map(anim => anim.finished));
  }

  /**
   * @abstract
   * @param { Animation } animation
   */
  cleanup(animation) {}


  // ---- Timing utility ----
  static sleep(time) {
    return new Promise(res => {
      setTimeout(res, time);
    });
  }
}
