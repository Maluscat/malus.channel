import { AnimationRunner } from './AnimationRunner.js';
import { KnockbackAnim } from './KnockbackAnim.js';

export class FadeInNarrowAnim extends AnimationRunner {
  static DURATION = 380;

  animations = [];

  /** @param { HTMLElement[] } elements */
  constructor(elements) {
    super();

    const anchorPos = /** @type {[number, number]} */ ([ document.body.clientWidth / 2, 0]);

    for (const elem of elements) {
      const pos = KnockbackAnim.getCenter(elem);
      const distance = KnockbackAnim.getDistance(pos, anchorPos);
      this.animations.push(new Animation(FadeInNarrowAnim.#getKeyframe(elem, distance)));
    }
  }

  static #getKeyframe(element, distance) {
    const delay = Math.pow(distance, .65) * 4.5;

    const keyframes = [
      {
        pointerEvents: 'none',
        opacity: 0,
        scale: 1.35,
        easing: 'cubic-bezier(.45,.05,.65,1)',
      }, {
        opacity: 1,
        offset: .645,
      }, {
        scale: .962,
        offset: .76,
        easing: 'cubic-bezier(.5,.05,.85,1)',
      }, {
        pointerEvents: 'none',
      },
    ];

    const options = {
      duration: this.DURATION,
      delay: delay,
      fill: 'backwards',
      easing: 'cubic-bezier(.1,.05,.75,1)'
    };

    return new KeyframeEffect(element, keyframes, options);
  }
}
