import { AnimationRunner } from './AnimationRunner.js';
import { KnockbackAnim } from './KnockbackAnim.js';

export class BounceAnim extends AnimationRunner {
  static DURATION = KnockbackAnim.DURATION * .7;

  animations;

  constructor(element) {
    super();
    this.animations = [ new Animation(BounceAnim.#getKeyframe(element)) ];
  }

  static #getKeyframe(element) {
    const keyframes = [
      {
        scale: 'none',
        easing: 'cubic-bezier(0,0,.35,1)',
      }, {
        scale: .92,
        offset: .42,
        easing: 'ease-in-out',
      }, {
        scale: 1.03,
        offset: .81,
        easing: 'ease-in-out',
      },
    ];

    const options = {
      duration: this.DURATION,
      easing: 'cubic-bezier(0,0,.55,1)'
    };

    return new KeyframeEffect(element, keyframes, options);
  }
}
