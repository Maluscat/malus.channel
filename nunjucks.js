import fs from 'node:fs';
import nodePath from 'node:path';
import Nunjucks from 'nunjucks';


let njk;
const compiledTemplates = new Map();

export function setupNunjucks(pageData) {
  njk = Nunjucks.configure('');
  addNunjucksGlobals(pageData);
}

function addNunjucksGlobals(pageData) {
  njk.addGlobal('pageDataStatic', pageData);
}

export function getTemplate(path) {
  if (compiledTemplates.has(path)) {
    return compiledTemplates.get(path);
  } else {
    const src = fs.readFileSync(path, { encoding: 'utf8' });
    const template = new Nunjucks.Template(src, njk, path, true);
    compiledTemplates.set(path, template);
    return template;
  }
}
